class Student
  attr_accessor :first_name, :last_name, :courses
  attr_reader :full_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @full_name = @first_name + " " + @last_name
  end

  def courses
    @courses
  end

  def enroll(new_course)
    @courses.each do |course|
      if course.conflicts_with?(new_course)
        raise 'There was an error'
      end
    end
    @courses << new_course unless @courses.include?(new_course)
    @courses.last.students << self
  end

  def course_load
    new_hash = {}
    @courses.each do |course|
      if new_hash.has_key?(course.department)
        new_hash[course.department] = new_hash[course.department] + course.credits
      else
        new_hash[course.department] = course.credits
      end
    end
    new_hash
  end
end
